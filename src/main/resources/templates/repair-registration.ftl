<!doctype html>
<html lang="en">
<head>
    <#include "partials/head.ftl">
    <title>Car Repair - Repair Registration</title>
</head>

<body>


<#include "partials/navbar.ftl">

<div class="jumbotron" style="padding: 4rem 2rem">
    <h1 style="text-align: center">Repair Registration</h1>

    <#import "/spring.ftl" as spring />
    <form id="registerRepairForm" onsubmit="this.validate()" action="/repairs/registration" name="registerRepairForm" method="post">
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputDate">Date</label>
                <@spring.bind "registerRepairForm.date"/>
                <input  type="date" value="${registerRepairForm.date!''}" class="form-control" width="270" placeholder="DD/MM/YYYY" name="date"/>
                <#list spring.status.errorMessages as error>
                    <div class="alert alert-danger" role="alert">${error}</div>
                </#list>
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputStatus">Status</label>
                <select id="inputStatus" class="form-control" name="status">
                    <#if '${registerRepairForm.status}' == 'ON_HOLD'>
                        <option selected>ON_HOLD</option>
                        <option>ON_PROGRESS</option>
                        <option>DONE</option>
                    <#elseif '${registerRepairForm.status}' == 'ON_PROGRESS' >
                        <option>ON_HOLD</option>
                        <option selected>ON_PROGRESS</option>
                        <option>DONE</option>
                    <#else >
                        <option>ON_HOLD</option>
                        <option>ON_PROGRESS</option>
                        <option selected>DONE</option>
                    </#if>
                </select>
            </div>
            <div class="form-group col-md-6">
                <label for="inputType">Type</label>
                <select id="inputType" class="form-control" name="type">
                    <#if '${registerRepairForm.type}'=='SMALL'>
                        <option selected>SMALL</option>
                        <option>HEAVY</option>
                    <#else>
                        <option>SMALL</option>
                        <option selected>HEAVY</option>
                    </#if>
                </select>
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputCost">Cost</label>
                <@spring.bind "registerRepairForm.cost"/>
                <input type="text" class="form-control" id="inputCost" value="${registerRepairForm.cost!''}" placeholder="Cost" name="cost">
                <#list spring.status.errors.getFieldErrors("cost") as error>
                    <div class="alert alert-danger" role="alert">${error.defaultMessage}</div>
                </#list>
            </div>
            <div class="form-group col-md-6">
                <label for="inputUserType">Owner</label>
                <@spring.bind "registerRepairForm.user"/>
                <select id="inputUserType" class="form-control" name="user" placeholder="Name Surname AFM">
                    <option selected></option>
                    <#list users as user>
                        <option value="${user.afm}">${user.name} ${user.surname} ${user.afm}</option>
                    </#list>
                </select>
                <#list spring.status.errorMessages as error>
                    <div class="alert alert-danger" role="alert">${error}</div>
                </#list>
                <#if errorInService??>
                        <div class="alert alert-danger" role="alert"><span>${errorInService}</span></div>
                </#if>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputDescription">Description</label>
                <@spring.bind "registerRepairForm.description"/>
                <textarea class="form-control" id="inputDescription" value="${registerRepairForm.description!''}" rows="3" placeholder="Description"
                          name="description"></textarea>
                <#list spring.status.errorMessages as error>
                    <div class="alert alert-danger" role="alert">${error}</div>
                </#list>

            </div>
        </div>
        <button type="submit" class="btn btn-primary">Register</button>
    </form>

</div>


<#include "partials/scripts.ftl">
<script src="/repairRegisterValidation.js"></script>

</body>
</html>
