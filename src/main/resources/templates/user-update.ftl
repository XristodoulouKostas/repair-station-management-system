<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <#include "partials/head.ftl">
    <title>User - Update</title>
</head>
<body>

<main role="main" class="container">

    <#include "partials/navbar.ftl">

    <div class="jumbotron" style="padding: 4rem 2rem">
        <h1 style="text-align: center">User Update</h1>

        <#import "/spring.ftl" as spring />
        <form id="registerUserForm" action="/users/update" name="userUpdateForm" method="post">

            <input type="hidden" class="form-control" width="270" name="id"
                   value="${registerUserForm.id}"/>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputName">Name</label>
                    <@spring.bind "registerUserForm.name"/>
                    <input type="text" class="form-control" id="inputName" placeholder="Name" value="${registerUserForm.name!''}" name="name">
                    <#list spring.status.errorMessages as error>
                    <div class="alert alert-danger" role="alert">${error}</div>
                    </#list>
                </div>
                <div class="form-group col-md-6">
                    <label for="inputSurname">Surname</label>
                    <@spring.bind "registerUserForm.surname"/>
                    <input type="text" class="form-control" id="inputSurname" placeholder="surname" value="${registerUserForm.surname!''}" name="surname">
                    <#list spring.status.errorMessages as error>
                        <div class="alert alert-danger" role="alert">${error}</div>
                    </#list>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputAFM">AFM</label>
                    <@spring.bind "registerUserForm.afm"/>
                    <input type="text" class="form-control" id="inputAFM" placeholder="AFM" value="${registerUserForm.afm!''}" name="afm">
                    <#list spring.status.errorMessages as error>
                        <div class="alert alert-danger" role="alert">${error}</div>
                    </#list>
                </div>
                <div class="form-group col-md-6">
                    <label for="inputAddress">Address</label>
                    <@spring.bind "registerUserForm.address"/>
                    <input type="text" class="form-control" id="inputAddress" placeholder="Address" value="${registerUserForm.address!''}" name="address">
                    <#list spring.status.errorMessages as error>
                    <div class="alert alert-danger" role="alert">${error}</div>
                    </#list>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputEmail">Email</label>
                    <@spring.bind "registerUserForm.email"/>
                    <input type="text" class="form-control" id="inputEmail" placeholder="Email" value="${registerUserForm.email!''}" name="email">
                    <#list spring.status.errorMessages as error>
                    <div class="alert alert-danger" role="alert">${error}</div>
                    </#list>
                </div>
                <div class="form-group col-md-6">
                    <label for="inputPassword">New Password</label>
                    <@spring.bind "registerUserForm.password"/>
                    <input type="password" class="form-control" id="inputPassword" placeholder="New Password" name="password">
                    <#list spring.status.errorMessages as error>
                    <div class="alert alert-danger" role="alert">${error}</div>
                    </#list>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputVehicleBrand">Vehicle Brand</label>
                    <@spring.bind "registerUserForm.vehicleBrand"/>
                    <input type="text" class="form-control" id="inputVehicleBrand" placeholder="Vehicle Brand" value="${registerUserForm.vehicleBrand!''}" name="vehicleBrand">
                    <#list spring.status.errorMessages as error>
                    <div class="alert alert-danger" role="alert">${error}</div>
                    </#list>
                </div>
                <div class="form-group col-md-6">
                    <label for="inputVehiclePlate">Vehicle Plate</label>
                    <@spring.bind "registerUserForm.vehiclePlate"/>
                    <input type="text" class="form-control" id="inputVehiclePlate" placeholder="Plate" value="${registerUserForm.vehiclePlate!''}" name="vehiclePlate">
                    <#list spring.status.errorMessages as error>
                    <div class="alert alert-danger" role="alert">${error}</div>
                    </#list>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="inputUserType">User Type</label>
                    <select id="inputUserType" class="form-control" name="userType">
                    <#if '${registerUserForm.userType}' == 'ADMIN'>
                            <option selected>ADMIN</option>
                            <option>OWNER</option>
                    <#else>
                            <option>ADMIN</option>
                            <option selected>OWNER</option>
                    </#if>
                    </select>
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Save</button>
            <p class="errorMsg" style="color: red">
                <strong>${errorMessage!''}</strong>
            </p>
        </form>

    </div>
</main>

<#include "partials/scripts.ftl">
<script src="/userRegisterValidation.js"></script>

</body>
</html>