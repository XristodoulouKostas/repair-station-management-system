<!doctype html>
<html lang="en">
<head>
    <#include "partials/head.ftl">
    <title>Car Repair - Repair Search</title>
</head>

<body>


<main role="main" class="container">

    <#include "partials/navbar.ftl">

    <div class="jumbotron" style="padding: 4rem 2rem">
        <h1 style="text-align: center">Repair Search</h1>

        <#import "/spring.ftl" as spring />
        <form id="repairSearchForm" onsubmit="return validateDates()" action="/repairs/search" name="repairSearchForm" method="post">
            <h3>Search criteria:</h3>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputDateFrom">Date from</label>
                    <@spring.bind "repairSearchForm.dateFrom"/>
                    <input type="date" class="form-control" id="dateFrom" value="${repairSearchForm.dateFrom!''}"width="270" placeholder="DD/MM/YYYY" name="dateFrom"/>
                    <#list spring.status.errors.getFieldErrors("dateFrom") as error>
                    <div class="alert alert-danger" role="alert">${error.defaultMessage}</div>
                    </#list>
                </div>

                <div class="form-group col-md-6">
                    <label for="inputDateTo">Date to</label>
                    <@spring.bind "repairSearchForm.dateTo"/>
                    <input type="date" class="form-control" id="dateTo" value="${repairSearchForm.dateTo!''}"width="270" placeholder="DD/MM/YYYY" name="dateTo"/>
                    <#list spring.status.errors.getFieldErrors("dateTo") as error>
                    <div class="alert alert-danger" role="alert">${error.defaultMessage}</div>
                    </#list>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputAFM">AFM</label>
                    <@spring.bind "repairSearchForm.afm"/>
                    <input type="text" name="afm" value="${repairSearchForm.afm!''}"class="form-control" id="inputAFM" placeholder="AFM">
                    <#list spring.status.errors.getFieldErrors("afm") as error>
                    <div class="alert alert-danger" role="alert">${error.defaultMessage}</div>
                    </#list>
                </div>

                <div class="form-group col-md-6">
                    <label for="inputVehiclePlate">Vehicle Plate</label>
                    <@spring.bind "repairSearchForm.vehiclePlate"/>
                    <input type="text" name="vehiclePlate" value="${repairSearchForm.vehiclePlate!''}"class="form-control" id="inputVehiclePlate"
                           placeholder="Vehicle Plate">
                    <#list spring.status.errors.getFieldErrors("vehiclePlate") as error>
                    <div class="alert alert-danger" role="alert">${error.defaultMessage}</div>
                    </#list>
                </div>
            </div>

            <button type="submit" class="btn btn-primary">Search</button>
        </form>

    </div>


</main>

<#include "partials/scripts.ftl">
<script src="/repairSearchValidation.js"></script>


</body>
</html>
