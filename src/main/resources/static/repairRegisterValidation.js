jQuery(function () {
    $('#registerRepairForm').validate({
        onfocusout: false,
        onkeyup: false,
        rules: {
            cost: {
                required: true,
                digits: true
            },
            description: {
                required: true,
                minlength: 5
            }
        },
        messages: {
            cost: {
                digits: "Only digits are accepted"
            },
            description: {
                minlength: "The size must be more than 5 characters"
            }
        }
    });
});