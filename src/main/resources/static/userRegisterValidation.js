jQuery(function () {
    $('#registerUserForm').validate({
        onfocusout: false,
        onkeyup: false,
        rules: {
            afm: {
                required: true,
                minlength: 9,
                maxlength: 9,
            },
            name: {
                required: true,
                minlength: 2,
                lettersonly: true
            },
            surname: {
                required: true,
                minlength: 2,
                lettersonly: true
            },
            address: {
                minlength: 3
            },
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 3
            },
            vehicleBrand: {
                required: true
            },
            vehiclePlate: {
                required: true,
                pattern: /^[a-zA-Z]{3}[0-9]{3,4}$/
            }
        },
        messages: {
            afm: {
                minlength: "AFM size must be 9",
                maxlength: "AFM size must be 9"
            },
            name: {
                minlength: "Size must be more than 2",
                lettersonly: "Only letters are accepted"
            },
            surname: {
                minlength: "Size must be more than 2",
                lettersonly: "Only letters are accepted"
            },
            address: {
                minlength: "Size must be more than 3"
            },
            email: {
                email: "Please enter a valid email"
            },
            password: {
                minlength: "Size must be more than 3"
            },
            vehiclePlate: {
                pattern: "Please enter a valid plate"
            }
        }
    });
});