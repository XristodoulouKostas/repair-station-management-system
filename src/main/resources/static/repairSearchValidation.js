jQuery(function() {
    $('#repairSearchForm').validate({
        rules: {
            afm: {
                minlength: 9,
                maxlength: 9
            },
            vehiclePlate: {
                pattern: /^[a-zA-Z]{3}[0-9]{3,4}$/
            }
        },
        messages: {
            afm: {
                minlength: "Size must be 9",
                maxlength: "Size must be 9"
            },
            vehiclePlate: {
                pattern: "Invalid plate"
            }
        }
    });
});
