package gr.project.future.team1.repository;

import gr.project.future.team1.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User,Long> {

    @Override
    List<User> findAll();

    @Override
    void deleteById(Long id);

    User findByAfm(String afm);

    User findByEmail(String email);

    User findByAfmOrEmail(String afm, String email);

    User findByAfmAndEmail(String afm, String email);

    User findById(long id);

    User save(User user);

    List<User> findAllByAfmOrEmail(String afm, String email);
}
