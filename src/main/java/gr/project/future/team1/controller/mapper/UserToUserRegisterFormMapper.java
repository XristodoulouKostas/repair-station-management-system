package gr.project.future.team1.controller.mapper;

import gr.project.future.team1.domain.User;
import gr.project.future.team1.form.RegisterUserForm;
import org.springframework.stereotype.Component;

@Component
public class UserToUserRegisterFormMapper {

    public RegisterUserForm mapToUserRegisterForm(User user) {
        RegisterUserForm form = new RegisterUserForm();
        form.setId(user.getId());
        form.setAfm(user.getAfm());
        form.setEmail(user.getEmail());
        form.setName(user.getName());
        form.setSurname(user.getSurname());
        form.setAddress(user.getAddress());
        form.setUserType(user.getUserType());
        form.setVehicleBrand(user.getVehicleBrand());
        form.setVehiclePlate(user.getVehiclePlate());
        form.setPassword(user.getPassword());
        return form;
    }

}
