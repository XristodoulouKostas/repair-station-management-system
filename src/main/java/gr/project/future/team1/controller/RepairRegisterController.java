package gr.project.future.team1.controller;

import gr.project.future.team1.controller.mapper.RepairToRegisterRepairMapper;
import gr.project.future.team1.enums.RepairStatus;
import gr.project.future.team1.enums.RepairType;
import gr.project.future.team1.form.RegisterRepairForm;
import gr.project.future.team1.model.UserModel;
import gr.project.future.team1.service.RepairServiceImpl;
import gr.project.future.team1.service.UserServiceImpl;
import gr.project.future.team1.validator.RegisterRepairFormValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
public class RepairRegisterController {

    @Autowired
    UserServiceImpl userServiceImpl;

    @Autowired
    RepairServiceImpl repairService;

    @Autowired
    RegisterRepairFormValidator registerRepairFormValidator;

    @Autowired
    RepairToRegisterRepairMapper repairToRegisterRepairMapper;

    @InitBinder("registerRepairForm")
    protected void initRegisterBinder(WebDataBinder webDataBinder){
        webDataBinder.addValidators(registerRepairFormValidator);
    }

    @GetMapping("/repairs/registration")
    public String login(Model model) {
        if(!model.containsAttribute("registerRepairForm")) {
            RegisterRepairForm registerRepairForm=new RegisterRepairForm();
            registerRepairForm.setType(RepairType.SMALL);
            registerRepairForm.setStatus(RepairStatus.ON_HOLD);
            model.addAttribute("registerRepairForm", registerRepairForm);
        }
        List<UserModel> users = userServiceImpl.findAll();
        model.addAttribute("users", users);
        return "repair-registration";
    }

    @PostMapping(value = "/repairs/registration")
    public String register(Model model, @Valid RegisterRepairForm registerRepairForm,BindingResult bindingResult) {
        List<UserModel> users = userServiceImpl.findAll();
        model.addAttribute("users", users);
        if(bindingResult.hasErrors()){
            return "repair-registration";
        }
        try {
            repairService.create(registerRepairForm);
        }catch (Exception e){
            model.addAttribute("errorInService","Invalid User");
            return "repair-registration";
        }
        return "redirect:/";
    }
}
