package gr.project.future.team1.exception;

import gr.project.future.team1.utils.GlobalAttributes;

public class UserAfmAndEmailExistsException extends RuntimeException {
    public UserAfmAndEmailExistsException(){
        super(GlobalAttributes.AFM_EMAIL_USED_MESSAGE);
    }
}
