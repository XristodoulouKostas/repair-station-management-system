package gr.project.future.team1.exception;

import gr.project.future.team1.utils.GlobalAttributes;

public class UserAfmExistsException extends RuntimeException {

    public UserAfmExistsException() {
        super(GlobalAttributes.USER_AFM_EXISTS);
    }


}
