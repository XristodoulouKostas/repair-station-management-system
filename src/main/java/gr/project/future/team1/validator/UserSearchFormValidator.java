package gr.project.future.team1.validator;

import gr.project.future.team1.form.UserSearchForm;
import gr.project.future.team1.utils.GlobalAttributes;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class UserSearchFormValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return UserSearchForm.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        UserSearchForm userSearchForm = (UserSearchForm) target;

        if (!userSearchForm.getAfm().isBlank() && !userSearchForm.getAfm().matches(GlobalAttributes.AFM_PATTERN)) {
            errors.rejectValue("afm", "WRONG_AFM", GlobalAttributes.AFM_PATTERN_MESSAGE);
        }
        if (!userSearchForm.getEmail().isBlank() && !userSearchForm.getEmail().matches(GlobalAttributes.MAIL_PATTERN)) {
            errors.rejectValue("email", "WRONG_EMAIL", GlobalAttributes.MAIL_PATTERN_MESSAGE);
        }
    }


}

