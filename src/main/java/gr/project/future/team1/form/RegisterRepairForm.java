package gr.project.future.team1.form;

import gr.project.future.team1.enums.RepairStatus;
import gr.project.future.team1.enums.RepairType;
import gr.project.future.team1.utils.GlobalAttributes;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.LocalDate;

public class RegisterRepairForm {
    private long id;

    @NotNull(message = GlobalAttributes.DATE_MESSAGE)
    @DateTimeFormat(pattern = GlobalAttributes.DATE_PATTERN)
    private LocalDate date;

    private RepairStatus status;

    private RepairType type;

    private String cost;

    @Size(min = 5, message = GlobalAttributes.DESCRIPTION_MESSAGE)
    private String description;

    @Pattern(regexp = GlobalAttributes.AFM_PATTERN, message = GlobalAttributes.USER_MESSAGE)
    @Size(min = GlobalAttributes.AFM_LENGTH, max = GlobalAttributes.AFM_LENGTH, message = GlobalAttributes.USER_MESSAGE)
    private String user;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public RepairStatus getStatus() {
        return status;
    }

    public void setStatus(RepairStatus status) {
        this.status = status;
    }

    public RepairType getType() {
        return type;
    }

    public void setType(RepairType type) {
        this.type = type;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }
}
